let game={
    name:"Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasur"],
    friends: {hoenn: ["May","Max"], 
             kanto: ["Brock","Misty"]},
    talk: function(){
        console.log("Pikachu! I choose you!");
    }
}

console.log(game);
console.log(game.name);
console.log(game["pokemon"]);
console.log("Result of talk method");
game.talk();




function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name+" health is now reduced to " +this.attack);
        target.health-=this.attack;
        }
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }
    
} 

    let pikachu = new Pokemon("Pikachu",12);
    let geodude = new Pokemon("Geodude",8);
    let mewtwo = new Pokemon("Mewtwo",100);
    console.log(pikachu);
    console.log(geodude);
    console.log(mewtwo);

    geodude.tackle(pikachu);
    console.log(pikachu);
   
   mewtwo.tackle(geodude);
   geodude.faint();

   console.log(geodude);